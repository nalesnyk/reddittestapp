package com.example.reddittest.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.reddittest.data.room.entity.TopDataReddit;
import com.example.reddittest.di.ActivityContext;
import com.example.reddittest.repository.RedditDateRepository;
import com.example.reddittest.repository.RedditDateRepositoryImp;
import com.example.reddittest.ui.adapters.RedditTopAdapter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    AppCompatActivity activity;

    public ActivityModule (AppCompatActivity activity){
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext(){
        return activity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(@ActivityContext Context context){
        return new LinearLayoutManager(context);
    }

    @Provides
    RedditTopAdapter provideRedditTopAdapter(){
       return new RedditTopAdapter(new ArrayList<TopDataReddit>());
    }

    @Provides
    RedditDateRepository provideRedditDateRepository(RedditDateRepositoryImp redditDateRepositoryImp){
        return redditDateRepositoryImp;
    }

    @Provides
    CustomTabsIntent provideCustomTabsIntent(){
        return new CustomTabsIntent.Builder().build();
    }
}
