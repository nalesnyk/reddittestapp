package com.example.reddittest.di.component;

import com.example.reddittest.di.PerActivity;
import com.example.reddittest.di.module.ActivityModule;
import com.example.reddittest.ui.fragments.RedditTopFragment;
import com.example.reddittest.ui.presenters.RedditTopPresenter;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(RedditTopFragment redditTopFragment);

    void inject(RedditTopPresenter redditTopPresenter);
}
