package com.example.reddittest.di.component;

import android.app.Application;
import android.content.Context;

import com.example.reddittest.MvpApp;
import com.example.reddittest.data.network.ApiServiceReddit;
import com.example.reddittest.data.room.AppDatabase;
import com.example.reddittest.data.room.dao.TopDataRedditDao;
import com.example.reddittest.di.ApplicationContext;
import com.example.reddittest.di.module.ApplicationModule;
import com.example.reddittest.di.module.NetworkModule;
import com.example.reddittest.di.module.RoomModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, RoomModule.class})
public interface AppComponent {

    void inject(MvpApp mvpApp);

    @ApplicationContext
    Context context();

    Application application();

    ApiServiceReddit apiServiceReddit();

    AppDatabase appDatabase();

    TopDataRedditDao topDataRedditDao();
}
