package com.example.reddittest.di.module;

import android.app.Application;
import android.content.Context;

import com.example.reddittest.di.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context providerContext(){
        return application;
    }

    @Provides
    Application provideApplication(){
        return application;
    }
}
