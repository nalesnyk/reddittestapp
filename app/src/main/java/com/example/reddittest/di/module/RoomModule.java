package com.example.reddittest.di.module;

import android.app.Application;

import androidx.room.Room;

import com.example.reddittest.data.room.AppDatabase;
import com.example.reddittest.data.room.dao.TopDataRedditDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private AppDatabase appDatabase;

    public RoomModule (Application application){
        appDatabase = Room.databaseBuilder(application, AppDatabase.class, "database").build();
    }

    @Singleton
    @Provides
    AppDatabase provideRoomDatabase(){
        return appDatabase;
    }

    @Singleton
    @Provides
    TopDataRedditDao provideTopDataRedditDao(AppDatabase appDatabase){
        return appDatabase.topDataRedditDao();
    }
}
