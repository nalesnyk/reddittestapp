package com.example.reddittest.data.network;

import com.example.reddittest.data.network.model.top.TopResponse;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServiceReddit {

    @GET("top/.json")
    Single<Response<TopResponse>> getTopDate(@Query("limit") int limit, @Query("after") String after);
}
