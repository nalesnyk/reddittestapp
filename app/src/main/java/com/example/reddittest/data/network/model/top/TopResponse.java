package com.example.reddittest.data.network.model.top;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }
}
