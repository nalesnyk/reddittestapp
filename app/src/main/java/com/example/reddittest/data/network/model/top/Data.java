package com.example.reddittest.data.network.model.top;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("children")
    @Expose
    private List<Children> children;

    @SerializedName("after")
    @Expose
    private String after;

    public List<Children> getChildren() {
        return children;
    }

    public String getAfter() {
        return after;
    }
}
