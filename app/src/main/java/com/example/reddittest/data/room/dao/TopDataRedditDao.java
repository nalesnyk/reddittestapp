package com.example.reddittest.data.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.reddittest.data.room.entity.TopDataReddit;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface TopDataRedditDao {

    @Insert
    void insert(List<TopDataReddit> topDataRedditList);

    @Query("DELETE FROM topdatareddit")
    void deleteAll();

    @Query("SELECT * FROM topdatareddit WHERE afterLocal = :after")
    Single<List<TopDataReddit>> getAll(String after);
}
