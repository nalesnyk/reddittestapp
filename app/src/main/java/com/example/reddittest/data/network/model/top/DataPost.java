package com.example.reddittest.data.network.model.top;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPost {

    @SerializedName("subreddit")
    @Expose
    private String subreddit;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("author")
    @Expose
    private String author;

    @SerializedName("created_utc")
    @Expose
    private double createdUtc;

    @SerializedName("num_comments")
    @Expose
    private int numComments;

    @SerializedName("score")
    @Expose
    private String score;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("url")
    @Expose
    private String url;

    public String getTitle() {
        return title;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getAuthor() {
        return author;
    }

    public double getCreatedUtc() {
        return createdUtc;
    }

    public int getNumComments() {
        return numComments;
    }

    public String getScore() {
        return score;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCreatedUtc(double createdUtc) {
        this.createdUtc = createdUtc;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
