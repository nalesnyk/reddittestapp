package com.example.reddittest.data.room.entity;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.reddittest.data.network.model.top.DataPost;

@Entity
public class TopDataReddit {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @Embedded
    DataPost dataPost;

    public String afterDate;

    public String afterLocal;

    public String localUrl;

    public TopDataReddit(DataPost dataPost, String afterDate, String afterLocal, String localUrl) {
        this.dataPost = dataPost;
        this.afterDate = afterDate;
        this.afterLocal = afterLocal;
        this.localUrl = localUrl;
    }

    public int getId() {
        return id;
    }

    public DataPost getDataPost() {
        return dataPost;
    }

    public String getAfterDate() {
        return afterDate;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public String getAfterLocal() {
        return afterLocal;
    }
}
