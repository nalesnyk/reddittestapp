package com.example.reddittest.data.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.reddittest.data.room.dao.TopDataRedditDao;
import com.example.reddittest.data.room.entity.TopDataReddit;

@Database(entities = {TopDataReddit.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TopDataRedditDao topDataRedditDao();
}
