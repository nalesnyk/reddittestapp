package com.example.reddittest.data.network.model.top;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Children {

    @SerializedName("data")
    @Expose
    private DataPost dataPost;

    public DataPost getDataPost() {
        return dataPost;
    }
}
