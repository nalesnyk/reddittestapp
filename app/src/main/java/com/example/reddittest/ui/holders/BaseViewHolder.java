package com.example.reddittest.ui.holders;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.reddittest.R;
import com.squareup.picasso.Picasso;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    private int mCurrentPosition;
    private View itemView;

    public BaseViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void onBind(int position) {
        mCurrentPosition = position;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public void picasso(final String urlAvatar, final ImageView imageView) {

        Picasso.with(itemView.getContext())
                .load(urlAvatar)
                .placeholder(R.drawable.ic_backround_white)
                .error(R.drawable.ic_imagenotfound)
                .into(imageView);
    }
}
