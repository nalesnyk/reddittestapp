package com.example.reddittest.ui.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.reddittest.MvpApp;
import com.example.reddittest.di.component.ActivityComponent;
import com.example.reddittest.di.component.DaggerActivityComponent;
import com.example.reddittest.di.module.ActivityModule;

public class BaseActivity extends AppCompatActivity {

    private static ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .appComponent(((MvpApp) getApplication()).getAppComponent())
                .build();
    }

    public static ActivityComponent getActivityComponent() {
        return activityComponent;
    }
}
