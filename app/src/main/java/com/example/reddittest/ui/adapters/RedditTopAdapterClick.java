package com.example.reddittest.ui.adapters;

public interface RedditTopAdapterClick {

    void postClick(String url);
}
