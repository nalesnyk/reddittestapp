package com.example.reddittest.ui.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.reddittest.repository.RedditDateRepository;
import com.example.reddittest.ui.activities.BaseActivity;
import com.example.reddittest.ui.views.RedditTopView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RedditTopPresenter extends MvpPresenter<RedditTopView> {

    @Inject
    public CompositeDisposable compositeDisposable;

    @Inject
    public RedditDateRepository redditDateRepository;

    private String afterPage;

    private int currentPage;

    private boolean isLastPage;

    public RedditTopPresenter() {
        BaseActivity.getActivityComponent().inject(this);
    }

    public void getRedditTopDate() {
        if (afterPage == null) afterPage = "null";
        compositeDisposable.add(redditDateRepository.getTopDate(10, afterPage)
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (currentPage >= 4) isLastPage = true;
                            if (result.size() != 0) {
                                afterPage = result.get(result.size() - 1).getAfterDate();
                            }
                            getViewState().hideLoading();
                            getViewState().changeDataTop(result, isLastPage);
                            currentPage++;
                        }, throwable -> getViewState().showError(throwable.getMessage())
                )
        );
    }

    public void clickPage(String url){
        getViewState().openPageBrowser(url);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        compositeDisposable.clear();
    }
}
