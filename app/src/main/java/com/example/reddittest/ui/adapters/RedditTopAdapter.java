package com.example.reddittest.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reddittest.R;
import com.example.reddittest.data.room.entity.TopDataReddit;
import com.example.reddittest.ui.holders.BaseViewHolder;
import com.example.reddittest.ui.holders.ProgressViewHolder;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RedditTopAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;
    private RedditTopAdapterClick redditTopAdapterClick;

    private List<TopDataReddit> topDataRedditList;

    public class TopViewHolder extends BaseViewHolder {

        @BindView(R.id.tx_author)
        TextView txAuthor;

        @BindView(R.id.tx_subreddit)
        TextView txSubreddit;

        @BindView(R.id.tx_count_comments)
        TextView txCountComments;

        @BindView(R.id.tx_rating)
        TextView txRating;

        @BindView(R.id.tx_date)
        RelativeTimeTextView txDate;

        @BindView(R.id.tx_title)
        TextView txTitle;

        @BindView(R.id.img_thumbnail)
        ImageView imgThubnail;

        TopViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                redditTopAdapterClick.postClick(topDataRedditList.get(getAdapterPosition()).getDataPost().getUrl());
            });
            ButterKnife.bind(this, itemView);
        }

        public void onBind(int position) {
            txAuthor.setText(topDataRedditList.get(position).getDataPost().getAuthor());
            txCountComments.setText(String.valueOf(topDataRedditList.get(position).getDataPost().getNumComments()));
            txRating.setText(String.valueOf(topDataRedditList.get(position).getDataPost().getScore()));
            txSubreddit.setText(topDataRedditList.get(position).getDataPost().getSubreddit());
            txTitle.setText(topDataRedditList.get(position).getDataPost().getTitle());
            txDate.setReferenceTime((long) (topDataRedditList.get(position).getDataPost().getCreatedUtc()) * 1000);
            picasso(topDataRedditList.get(position).getDataPost().getThumbnail(), imgThubnail);
        }
    }

    public RedditTopAdapter(List<TopDataReddit> topDataRedditList) {
        this.topDataRedditList = topDataRedditList;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LOADING:
                return new ProgressViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading, viewGroup, false));
            case VIEW_TYPE_NORMAL:
                return new TopViewHolder(
                        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_top_post, viewGroup, false));
        }
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_top_post, viewGroup, false);
        return new TopViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder viewHolder, final int position) {
        viewHolder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            if (position == topDataRedditList.size() - 1) {
                return VIEW_TYPE_LOADING;
            } else {
                return VIEW_TYPE_NORMAL;
            }
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return topDataRedditList.size();
    }

    public void addLoading() {
        isLoaderVisible = true;
        topDataRedditList.add(new TopDataReddit(null, null, null, null));
        notifyItemInserted(topDataRedditList.size() - 1);
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = topDataRedditList.size() - 1;
        TopDataReddit item = topDataRedditList.get(position);
        if (item != null) {
            topDataRedditList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addItems(List<TopDataReddit> topDataRedditList) {
        this.topDataRedditList.addAll(topDataRedditList);
        notifyDataSetChanged();
    }

    public void setRedditTopAdapterClick(RedditTopAdapterClick redditTopAdapterClick) {
        this.redditTopAdapterClick = redditTopAdapterClick;
    }
}
