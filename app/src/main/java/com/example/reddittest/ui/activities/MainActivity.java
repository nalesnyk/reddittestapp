package com.example.reddittest.ui.activities;

import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.reddittest.R;
import com.example.reddittest.ui.fragments.RedditTopFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openRedditTopFragment();
    }

    private void openRedditTopFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_holder, RedditTopFragment.newInstance());
        ft.addToBackStack(null);
        ft.commit();
    }
}
