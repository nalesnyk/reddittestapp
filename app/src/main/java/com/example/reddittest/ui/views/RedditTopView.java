package com.example.reddittest.ui.views;

import com.example.reddittest.data.room.entity.TopDataReddit;

import java.util.List;

public interface RedditTopView extends BaseView {

    void changeDataTop(List<TopDataReddit> topDataRedditList, boolean isLastPage);

    void openPageBrowser(String url);
}
