package com.example.reddittest.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.reddittest.R;
import com.example.reddittest.data.room.entity.TopDataReddit;
import com.example.reddittest.di.ActivityContext;
import com.example.reddittest.ui.adapters.RedditTopAdapter;
import com.example.reddittest.ui.adapters.RedditTopAdapterClick;
import com.example.reddittest.ui.presenters.RedditTopPresenter;
import com.example.reddittest.ui.views.RedditTopView;
import com.example.reddittest.utils.PaginationListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RedditTopFragment extends BaseFragment implements RedditTopView, RedditTopAdapterClick {

    @Inject
    RedditTopAdapter redditTopAdapter;

    @Inject
    @ActivityContext
    Context context;

    @Inject
    LinearLayoutManager linearLayoutManagerRedditTop;

    @Inject
    CustomTabsIntent customTabsIntent;

    @InjectPresenter
    RedditTopPresenter redditTopPresenter;

    @BindView(R.id.recycler_top)
    RecyclerView recyclerTop;

    @BindView(R.id.progress)
    ProgressBar progress;

    private boolean isLastPage = false;
    private boolean isLoading = false;
    private boolean isStartPage = true;

    public static RedditTopFragment newInstance() {

        Bundle args = new Bundle();

        RedditTopFragment fragment = new RedditTopFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_top, container, false);
        setUnBinder(ButterKnife.bind(this, view));

        recyclerTop.setLayoutManager(linearLayoutManagerRedditTop);
        recyclerTop.setHasFixedSize(true);
        recyclerTop.setAdapter(redditTopAdapter);
        redditTopAdapter.setRedditTopAdapterClick(this);
        recyclerTop.addOnScrollListener(new PaginationListener(linearLayoutManagerRedditTop) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                isStartPage = false;
                redditTopPresenter.getRedditTopDate();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        redditTopPresenter.getRedditTopDate();
        return view;
    }

    @Override
    public void changeDataTop(List<TopDataReddit> topDataRedditList, boolean isLastPage) {
        if (!isStartPage) redditTopAdapter.removeLoading();
        redditTopAdapter.addItems(topDataRedditList);
        hideLoading();
        this.isLastPage = isLastPage;
        if(!isLastPage) redditTopAdapter.addLoading();
        isLoading = false;
    }

    @Override
    public void openPageBrowser(String url) {
        customTabsIntent.launchUrl(context, Uri.parse(url));
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        progress.setVisibility(View.GONE);
        recyclerTop.setVisibility(View.VISIBLE);
    }

    @Override
    public void postClick(String url) {
        redditTopPresenter.clickPage(url);
    }
}
