package com.example.reddittest.ui.views;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;

public interface BaseView extends MvpView {

    void showLoading();

    void hideLoading();

    void showError(@StringRes int resId);

    void showError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);
}
