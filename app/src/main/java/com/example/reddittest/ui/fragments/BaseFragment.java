package com.example.reddittest.ui.fragments;

import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.example.reddittest.di.component.ActivityComponent;
import com.example.reddittest.ui.activities.BaseActivity;
import com.example.reddittest.ui.views.BaseView;

import butterknife.Unbinder;

public class BaseFragment extends MvpAppCompatFragment implements BaseView {

    private Unbinder mUnBinder;

    public ActivityComponent getActivityComponent() {
            return BaseActivity.getActivityComponent();
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(int resId) {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showMessage(int resId) {

    }
}
