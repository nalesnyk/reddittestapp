package com.example.reddittest.repository;

import com.example.reddittest.data.network.ApiServiceReddit;
import com.example.reddittest.data.room.dao.TopDataRedditDao;

public interface BaseRepository {

    ApiServiceReddit getApiServiceReddit();

    TopDataRedditDao getTopRedditDao();
}
