package com.example.reddittest.repository;

import com.example.reddittest.data.room.entity.TopDataReddit;

import java.util.List;

import io.reactivex.Single;

public interface RedditDateRepository extends BaseRepository {

    Single<List<TopDataReddit>> getTopDate(int limit, String after);
}
