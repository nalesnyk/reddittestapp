package com.example.reddittest.repository;

import com.example.reddittest.data.network.ApiServiceReddit;
import com.example.reddittest.data.network.model.top.Children;
import com.example.reddittest.data.network.model.top.Data;
import com.example.reddittest.data.room.dao.TopDataRedditDao;
import com.example.reddittest.data.room.entity.TopDataReddit;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RedditDateRepositoryImp extends BaseRepositoryImp implements RedditDateRepository {

    @Inject
    public RedditDateRepositoryImp(ApiServiceReddit apiServiceReddit, TopDataRedditDao topDataRedditDao) {
        super(apiServiceReddit, topDataRedditDao);
    }

    @Override
    public Single<List<TopDataReddit>> getTopDate(int limit, String after) {
        return getApiServiceReddit().getTopDate(limit, after)
                .flatMap(topResponse -> {
                    if (topResponse.isSuccessful()) {
                        if (after.equals("null")) getTopRedditDao().deleteAll();
                        if (topResponse.body() != null) {
                            List<TopDataReddit> topDataRedditList = converToToDataReddit(topResponse.body().getData(), after);
                            getTopRedditDao().insert(topDataRedditList);
                            return Single.just(topDataRedditList);
                        }
                    }
                    return getTopRedditDao().getAll(after);
                })
                .onErrorResumeNext(throwable -> getTopRedditDao().getAll(after));
    }

    private List<TopDataReddit> converToToDataReddit(Data data, String afterLocal) {
        List<TopDataReddit> topDataRedditList = new ArrayList<>();
        for (Children children : data.getChildren()) {
            topDataRedditList.add(new TopDataReddit(children.getDataPost(),
                    data.getAfter(), afterLocal, null));
        }
        return topDataRedditList;
    }
}
