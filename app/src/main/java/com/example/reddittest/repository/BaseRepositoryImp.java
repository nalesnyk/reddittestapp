package com.example.reddittest.repository;

import com.example.reddittest.data.network.ApiServiceReddit;
import com.example.reddittest.data.room.dao.TopDataRedditDao;

import javax.inject.Inject;

public class BaseRepositoryImp implements BaseRepository {

    private final ApiServiceReddit apiServiceReddit;
    private TopDataRedditDao topDataRedditDao;

    @Inject
    public BaseRepositoryImp(ApiServiceReddit apiServiceWeather, TopDataRedditDao topDataRedditDao) {
        this.apiServiceReddit = apiServiceWeather;
        this.topDataRedditDao = topDataRedditDao;
    }

    @Override
    public ApiServiceReddit getApiServiceReddit() {
        return apiServiceReddit;
    }

    @Override
    public TopDataRedditDao getTopRedditDao() {
        return topDataRedditDao;
    }


}
