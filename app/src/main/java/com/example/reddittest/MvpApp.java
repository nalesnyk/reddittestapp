package com.example.reddittest;

import android.app.Application;

import com.example.reddittest.di.component.AppComponent;
import com.example.reddittest.di.component.DaggerAppComponent;
import com.example.reddittest.di.module.ApplicationModule;
import com.example.reddittest.di.module.RoomModule;

public class MvpApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .roomModule(new RoomModule(this))
                .build();

        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
